﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TESTBS23.Models
{
    public class CommentViewModels : PostViewModels
    {
        public CommentViewModels()
        {
           
        }

        public int CommentId { get; set; }

        public string CommentText { get; set; }

        public DateTime? CommentCreateDateUtc { get; set; }

        public int? LikeCount { get; set; }

        public int? DislikeCount { get; set; }

        public string CommentUser { get; set; }
    }
}