﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using TESTBS23.DataModel;

namespace TESTBS23.Models
{
    public class PostViewModels
    {
        public PostViewModels()
        {
            
        }

        public int Id { get; set; }

        public string PostTitle { get; set; }

        public DateTime? CreateDateUtc { get; set; }

        public string CommentCount { get; set; }

        public string PostUser { get; set; }

    }

}