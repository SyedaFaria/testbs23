﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TESTBS23.DataModel;
using TESTBS23.Models;

namespace TESTBS23.Services
{
    public class PostServices
    {
        private Entities context;

        public PostServices()
        {
            context = new Entities();
        }

        public IEnumerable<CommentViewModels> GetAllPosts(string searchKey, int pageSize, int pageNo)
        {
            List<CommentViewModels> posts = new List<CommentViewModels>();

            var postQ = context.Comments.Select(c => new CommentViewModels()
            {
                Id = c.Post1.Id,
                PostTitle = c.Post1.PostTitle,
                CreateDateUtc = c.Post1.CreateDateUtc,
                CommentCount = c.Post1.Comments.Count() + "Comment(s)",

                CommentId = c.Id,
                CommentText = c.CommentText,
                CommentCreateDateUtc = c.CreateDateUtc,
                LikeCount = c.LikeCount,
                DislikeCount = c.DislikeCount
            });

            if (searchKey != "" && searchKey != null)
            {
                postQ = postQ.Where(w => w.PostTitle.Contains(searchKey)).OrderBy(o => o.Id);
            }
            if (pageSize != 0 && pageNo != -1)
            {
                postQ = postQ.OrderBy(o => o.Id).Skip(pageSize * pageNo).Take(pageSize);
            }

            return postQ;

        }
    }
}