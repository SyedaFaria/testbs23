﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Text.Json;
using System.Text.Json.Serialization;
using TESTBS23.DataModel;
using TESTBS23.Models;
using TESTBS23.Services;

namespace TESTBS23.Controllers
{
    public class PostAPIController : ApiController
    {
        private PostServices postServices;

        public PostAPIController()
        {
            postServices = new PostServices();
        }

        // GET: api/PostAPI
        public IHttpActionResult Get()
        {
            var postJsonString = postServices.GetAllPosts("", 0, -1).ToList();

            return Json( new {
                data = postJsonString,
                recordsTotal = postJsonString.Count(),
                recordsFiltered = postJsonString.Count()
            });
        }

        // GET: api/PostAPI/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/PostAPI
        public IHttpActionResult Post(DataTableViewModel dtModel)
        {
            var postJsonString = postServices.GetAllPosts(searchKey:dtModel.search.value, pageSize:dtModel.length, pageNo:dtModel.start).ToList();

            return Json(new
            {
                data = postJsonString,
                recordsTotal = postJsonString.Count(),
                recordsFiltered = postJsonString.Count()
            });

        }

        // PUT: api/PostAPI/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/PostAPI/5
        public void Delete(int id)
        {
        }
    }
}
