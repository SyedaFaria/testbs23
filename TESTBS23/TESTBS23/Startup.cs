﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TESTBS23.Startup))]
namespace TESTBS23
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
